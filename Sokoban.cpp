// Sokoban.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <conio.h>
#include "Scene.h"
#include "Game.h"

using namespace std;

void InvisibleCursor(){
	HANDLE hCons = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO cursor = { 1, false };
	SetConsoleCursorInfo(hCons, &cursor);
}

void Play(){
	InvisibleCursor();
	string fileName = "Maps\\level1.txt";
	cout << "You can control player by arrows. For exit press ESC. For restart Enter." << endl;
	cout << "For start press Enter" << endl;
	char key = _getch();
	while ((int)key != 13 && (int)key != 27){
		key = _getch();
	}
	bool b = false;
	if (key == 13){
		system("cls");
		Scene sc(fileName);
		b = sc.Play();
		while (b){
			system("cls");
			b = Scene(fileName).Play();
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	Game().Play();
	//Play();
	system("pause");
	return 0;
}

