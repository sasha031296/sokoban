﻿#pragma once
#include <iostream>
#include <fstream>
//#include <cstdlib>
#include <string>
#include "Point.h"

using namespace std;

class Matrix
{
private:
	string FileName;
	//int width;
	//int height;
	//int** masInt;
	//int** mas;
	//char** masChar; //для красивого вывода
	char symbols[5];
public:
	int width;
	int height;
	int** mas;

	Matrix();
	Matrix(string s);
	void TakeSize();
	void InitializeMassive();
	void InputMassive();
	void Print();
	bool IsEmpty(Point p);
	~Matrix();
};

enum TypeOfCell{
	Empty = 0,
	Wall = 1,
	DotType = 2, 
	BoxType = 3,
	PlayerType = 4
};

