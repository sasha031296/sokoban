﻿#include "stdafx.h"
#include "Player.h"


Player::Player() : Actor()
{
}

Player::Player(Point p) : Actor(p, (char)1){

}
void Player::ReDraw(Direction dir){

}

Box* Player::ReDraw(Direction dir, Box* boxes, int len, Dot* dots, int dotLen){
	Erase();
	for (int i = 0; i < dotLen; i++){
		if (dots[i].GetPoint() == _point){
			dots[i].Draw();
		}
	}
	_point = _point + Point(dir);
	/*Point(0, 15).SetCursor();
	cout << "Player: " << _point.x << ", " << _point.y << endl;*/
	int t = 10;
	for (int i = 0; i < len; i++){
		Point(0, t).SetCursor();
		//cout << boxes[i].GetPoint().x << ", " << boxes[i].GetPoint().y << endl;
		t++;
		if (boxes[i].GetPoint() == _point){
			//если хочешь сделать изменение цвета, то сделай тут. передавай в функцию ReDraw() массив точек и если совпадает, то выводи другим цветом.
			//boxes[i].ReDraw(dir, dots, dotLen); //чтоб цвет менять, но не идет
			boxes[i].ReDraw(dir);
		}
	}
	Draw();
	return boxes;
}

bool Player::CanMove(Matrix m, Direction d){
	return m.IsEmpty(_point + Point(d));
}

bool Player::CanMove(Matrix m, Direction d, Box* boxes, int len){
	if (!m.IsEmpty(_point + Point(d))){ //если в том напавлении стена
		return false;
	}
	bool b = true;
	for (int i = 0; i < len; i++){
		if (boxes[i].GetPoint() == (this->_point + Point(d))){ //если в том направлении есть коробка, нужно проверять, нет ли еще одной
			if (!m.IsEmpty(boxes[i].GetPoint() + Point(d))){ //если после коробки стена
				return false;
			}
			for (int j = 0; j < len; j++){
				if ((i != j) && (boxes[j].GetPoint() == (boxes[i].GetPoint() + Point(d)))) //если после коробки коробка
					return false;
			}
		}
	}
	return true;
}

Player::~Player()
{
}
