#pragma once
#include <string>
#include <conio.h>
#include "Matrix.h"
#include "Actor.h"
#include "Box.h"
#include "Player.h"
#include "Dot.h"

using namespace std;

class Scene
{
private:
	//string _fileName;
	Matrix _matrix;
	int _dotCount;
	int _boxCount;
	Dot* _dots;
	Box* _boxes;
	Player _player;

public:
	Scene();
	Scene(string);
	void InitializaActors();
	int Play();
	void ReDraw(Direction);
	bool IsWin();
	Direction GetDirection(char);
	~Scene();
};

