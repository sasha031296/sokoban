#pragma once
#include "Actor.h"
#include "Box.h"
#include "Dot.h"

class Player :
	public Actor
{
public:
	Player();
	Player(Point p);
	virtual void ReDraw(Direction);
	Box* ReDraw(Direction, Box*, int, Dot*, int);
	bool virtual CanMove(Matrix, Direction);
	bool virtual CanMove(Matrix, Direction, Box*, int);
	~Player();
};

