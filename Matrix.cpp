﻿#include "stdafx.h"
#include "Matrix.h"
#include <string>
#include <ctime>


Matrix::Matrix()
{
}

Matrix::Matrix(string FName){
	/*
	symbols[0] = '☺';
	symbols[1] = '█';
	symbols[2] = '•';
	symbols[3] = '◘';
	symbols[4] = '☺';
	*/
	symbols[0] = ' ';
	symbols[1] = (char)(-37);
	symbols[2] = (char)(-6);
	symbols[3] = (char)(253);
	symbols[4] = (char)1;
	FileName = FName;
	TakeSize();
	InitializeMassive();
	InputMassive();
}

void Matrix::TakeSize(){
	ifstream in;
	string s;
	in.open(FileName);
	width = 0;
	height = 0;
	//char *mas = new char [n];
	while (getline(in, s)){
		if (width < s.length()){
			width = s.length();
		}
		height++;
	}
	in.close();
}

void Matrix::InitializeMassive(){
	mas = new int*[height];
	srand(time(0));
	//masInt = new int*[height];
	for (int i = 0; i < height; i++){
		mas[i] = new int[width];
		//masInt[i] = new int[width];
	}
}

void Matrix::InputMassive(){
	ifstream in;
	string s;
	in.open(FileName);
	int pl = 0;
	while (getline(in, s)){
		for (int j = 0; j < s.length(); j++){
			//masInt[pl][j] = (int)(s[j] - '0');
			mas[pl][j] = (int)(s[j] - '0');
		}
		for (int i = s.length(); i < width; i++){
			//masInt[pl][i] = 1;
			mas[pl][i] = 1/*(int)Wall*/;
		}
		pl++;
	}
	in.close();
}

void Matrix::Print(){
	for (int i = 0; i < height; i++){
		for (int j = 0; j < width; j++){
			cout << symbols[mas[i][j]];
		}
		cout << endl;
	}
}

bool Matrix::IsEmpty(Point p){
	return (mas[p.y][p.x] != (int)Wall);
}

Matrix::~Matrix()
{
}
