#include "stdafx.h"
#include "Scene.h"


Scene::Scene()
{
}

Scene::Scene(string s){
	//_fileName = s;
	_matrix = Matrix(s);
	InitializaActors();
}

void Scene::InitializaActors(){
	_dotCount = 0;
	_boxCount = 0;
	int pl = 0;
	for (int h = 0; h < _matrix.height; h++){
		for (int w = 0; w < _matrix.width; w++){
			switch ((Direction)(_matrix.mas[h][w])){
			case DotType:
				_dotCount++;
				break;
			case BoxType:
				_boxCount++;
				break;
			case PlayerType:
				pl++;
				break;
			}
		}
	}
	if (pl != 1){
		cout << "wrong file!!!" << endl;
	}
	_dots = new Dot[_dotCount];
	_boxes = new Box[_boxCount];
	int d = 0, b = 0;
	for (int h = 0; h < _matrix.height; h++){
		for (int w = 0; w < _matrix.width; w++){
			Point p(w, h);
			switch ((Direction)(_matrix.mas[h][w])){
			case DotType:
				_dots[d] = Dot(p);
				d++;
				break;
			case BoxType: 
				_boxes[b] = Box(p);
				b++;
				break;
			case PlayerType:
				_player = Player(p);
				break;
			}
		}
	}
}

//0 - �������, 1 - ������, 2 - �����
int Scene::Play(){
	char key;
	_matrix.Print();
	key = _getch();
	while ((int)key != 27){
		if (_player.CanMove(_matrix, GetDirection(key), _boxes, _boxCount) && GetDirection(key) != None){
			ReDraw(GetDirection(key));
		}
		if (IsWin()){
			return 1;
		}
		if ((int)key == 13){ //������ �������
			return 0;
		}
		if ((int)key == 27){ //������ �����
			return 2;
		}
		key = _getch();
	}
	//Point(0, _matrix.height + 1).SetCursor();
	if (IsWin()){
		return 1;
	}
	return 2;
}

void Scene::ReDraw(Direction dir){
	_boxes = _player.ReDraw(dir, _boxes, _boxCount, _dots, _dotCount); //��� ����, ������ ��� �������� ���������� �������
}

Direction Scene::GetDirection(char c){
	switch ((int)c)
	{
	case 72: return Up;
	case 75 :return Left;
	case 77: return Right; 
	case 80: return Down;
	default:
		return None;
	}
}

bool Scene::IsWin(){
	bool t = false;
	for (int i = 0; i < _dotCount; i++){
		for (int j = 0; j < _boxCount; j++){ //��������� ��������� �� ��� ����� ���� ���� � ����� ��������
			if (_dots[i].IsMatch(_boxes[j]))
				t = true;
		}
		if (!t){ //���� ���� ���� ����� �� ������, �� �� �������
			return false;
		}
		t = false;
	}
	return true;
}

Scene::~Scene()
{
}
