#pragma once
#include "Actor.h"
#include "Dot.h"

class Box :
	public Actor
{
public:
	Box();
	Box(Point p);
	void virtual ReDraw(Direction);
	void ReDraw(Direction, Dot*, int);
	bool virtual CanMove(Matrix, Direction);
	~Box();
};

