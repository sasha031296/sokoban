#include "stdafx.h"
#include "Point.h"


Point::Point()
{
	x = 0;
	y = 0;
}

Point::Point(int a, int b)
{
	x = a;
	y = b;
}

Point::Point(Direction d){
	x = 0, y = 0;
	switch (d)
	{
	case Up:
		y--;
		break;
	case Right:
		x++;
		break;
	case Down:
		y++;
		break;
	case Left:
		x--;
		break;
	default:
		break;
	}
}

Point::~Point()
{
}

void Point::SetCursor(){
	COORD coord;
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(
		GetStdHandle(STD_OUTPUT_HANDLE),
		coord
		);
}

bool Point::operator == (Point p){
	return (x == p.x && y == p.y);
}

bool Point::operator != (Point p){
	return !(x == p.x && y == p.y);
}

Point Point::operator + (Point p){
	Point r(x + p.x, y + p.y);
	return r;
}
Point Point::operator - (Point p){
	Point r(this->x - p.x, this->y - p.y);
	return r;
}
/*Point Point::operator + (Direction d){
	Point p(x, y);
	switch (d)
	{
	case Up:
		p.y++;
		break;
	case Right:
		p.x++;
		break;
	case Down:
		p.y--;
		break;
	case Left:
		p.x--;
		break;
	default:
		break;
	}
	return p;
}*/
