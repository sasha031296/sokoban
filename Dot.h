#pragma once
#include "Point.h"
#include "Actor.h"

class Dot
{
private:
	Point _point;
	char _face;

public:
	Dot();
	Dot(Point);
	void Draw();
	bool IsMatch(Point);
	bool IsMatch(Actor);
	void ReDraw();
	Point GetPoint();
	~Dot();
};

